#include <stdio.h>

 int main() {

	 int x=0;
	 int y=0;

  printf("\nEnter first integer :");
  scanf("%d",&x);
  printf("Enter second integer :");
  scanf("%d",&y);

   printf("\nIntegers before swapping : %d , %d\n",x,y);

   x=x+y;
   y=x-y;
   x=x-y;

   printf("Integers after swapping : %d , %d\n\n",x,y);

   return 0;

}
